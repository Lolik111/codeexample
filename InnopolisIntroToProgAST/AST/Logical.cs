﻿using System;
using System.Configuration;
using System.Xml.Linq;
using InnopolisIntroToProgAST.Operators;

namespace InnopolisIntroToProgAST
{
    public class Logical : LogicalExpression
    {
        public Logical(LogicalOperator op, LogicalExpression left, LogicalExpression right)
        {
            Op = op;
            Left = left;
            Right = right;
        }


        protected Logical()
        {
            this.Left = null;
            this.Op = null;
            this.Right = null;
        }

        public LogicalOperator Op { get; protected set; }

        public LogicalExpression Left { get; protected set; }

        public LogicalExpression Right { get; protected set; }


        public override bool Compute()
        {
            return Op.Apply(Left, Right);
        }

        public override XElement ToXml()
        {
            XElement node = new XElement("Logical", new XAttribute("Operator", Op.StringOp));
            node.Add(Left.ToXml());
            node.Add(Right.ToXml());
            return node;
        }
    }
}