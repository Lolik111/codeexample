﻿using System;

namespace InnopolisIntroToProgAST.Operators
{
    public class TermOperator : Sign
    {

        private TermOperator(string str)
        {
            StringOp = str;
        }

        public static TermOperator Create(string str)
        {
            switch (str)
            {
                case "+":
                case "-":
                case "*":
                case "/":
                    return new TermOperator(str);
                default:
                    return null;
            }
        }

        public static bool IsSymbol(char c)
        {
            if (c == '-' || c == '+' || c == '*' || c == '/')
            {
                return true;
            }
            return false;
        }

        public override int Evaluate()
        {
            switch (StringOp)
            {
                case "+":
                case "-":
                    return 100;
                case "*":
                case "/":
                    return 110;
            }
            return -1;
        }

        public long Apply(ArithmeticExpression left, ArithmeticExpression right)
        {
            switch (StringOp)
            {
                case "+":
                    return left.Compute() + right.Compute();
                case "-":
                    return left.Compute() - right.Compute();
                case "*":
                    return left.Compute() * right.Compute();
                case "/":
                    return left.Compute() / right.Compute();
                default:
                    throw new Exception("Wrong operator");
            }
        }
    }
}