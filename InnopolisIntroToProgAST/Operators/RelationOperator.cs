﻿using System;

namespace InnopolisIntroToProgAST.Operators
{
    public class RelationOperator : Sign
    {
        private RelationOperator(string str)
        {
            StringOp = str;
        }

        public static RelationOperator Create(string str)
        {
            switch (str)
            {
                case "<":
                case "<=":
                case "==":
                case "!=":
                case ">":
                case ">=":
                    return new RelationOperator(str);
                default:
                    return null;
            }
        }

        public static bool IsSymbol(char c)
        {
            return c == '<' || c == '=' || c == '>';
        }

        public override int Evaluate()
        {
            return 10;
        }

        public bool Apply(ArithmeticExpression left, ArithmeticExpression right)
        {
            switch (StringOp)
            {
                case "<":
                    return left.Compute() < right.Compute();
                case "<=":
                    return left.Compute() <= right.Compute();
                case "==":
                    return left.Compute() == right.Compute();
                case "!=":
                    return left.Compute() != right.Compute();
                case ">":
                    return left.Compute() > right.Compute();
                case ">=":
                    return left.Compute() >= right.Compute();
                default:
                    throw new Exception("Wrong operator");
            }
        }
         
    }
}