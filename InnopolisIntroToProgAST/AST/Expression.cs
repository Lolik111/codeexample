﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using InnopolisIntroToProgAST.Operators;

namespace InnopolisIntroToProgAST
{
    public abstract class Expression
    {
        public static Expression Generate(Sign s, Expression left, Expression right)
        {
            if (left is ArithmeticExpression && right is ArithmeticExpression && s is TermOperator)
            {
                return new Term((TermOperator)s,  (ArithmeticExpression) left,  (ArithmeticExpression) right);
            }
            if (left is ArithmeticExpression && right is ArithmeticExpression && s is RelationOperator)
            {
                return new Relation((RelationOperator)s, (ArithmeticExpression) left, (ArithmeticExpression) right);
            }
            if (left is LogicalExpression && right is LogicalExpression && s is LogicalOperator)
            {
                return new Logical((LogicalOperator)s, (LogicalExpression) left, (LogicalExpression) right);
            }
            return null;
        }

        public abstract XElement ToXml();

        public object Calculate()
        {
            if (this is ArithmeticExpression)
            {
                return ((ArithmeticExpression)this).Compute();
            }
            if (this is LogicalExpression)
            {
                return ((LogicalExpression)this).Compute();
            }
            return null;
        }

        public static Expression LoadXmlFile(string fileName)
        {
            XDocument document = XDocument.Load(fileName);
            if (document.Root == null || document.Root.Name.LocalName != "Tree")
            {
                return null;
            }
            Expression result = Traverse(document.Root.Elements().First());
            return result;
        }

        private static Expression Traverse(XElement node)
        {
            switch (node.Name.LocalName)
            {
                case "Integer":
                    long l;
                    long.TryParse(node.Value, out l);
                    return new Integer(l);
                case "Logical":
                case "Relation":
                case "Term":
                    Sign op = GetOperator(node.FirstAttribute);
                    IEnumerator<XElement> e = node.Elements().GetEnumerator();
                    e.MoveNext();
                    Expression first = Traverse(e.Current);
                    e.MoveNext();
                    Expression second = Traverse(e.Current);
                    return Expression.Generate(op, first, second);
                default:
                    return null;
            }
        }


        public static void WriteToXmlFile(string fileName, Expression expression)
        {
            XDocument document = new XDocument();
            XElement element = new XElement("Tree");
            document.Add(element);
            element.Add(expression.ToXml());
            document.Save(fileName);
        }

        private static Sign GetOperator(XAttribute attribute)
        {
            return attribute.Name.LocalName == "Operator" ? Sign.Generate(attribute.Value) : null;
        }
    }
}