﻿namespace InnopolisIntroToProgAST.Operators
{
    public abstract class Sign
    {
        public string StringOp { get; protected set; }

        public static Sign Generate(string str)
        {
            switch (str)
            {
                case "||":
                case "|":
                case "&&":
                case "&":
                    return LogicalOperator.Create(str);
                case "<":
                case "<=":
                case "==":
                case "!=":
                case ">":
                case ">=":
                    return RelationOperator.Create(str);
                case "+":
                case "-":
                case "*":
                case "/":
                    return TermOperator.Create(str);
                case "(":
                case ")":
                    return Bracket.Create(str);
                default:
                    return null;
            }
        }

        public abstract int Evaluate();

    }
}