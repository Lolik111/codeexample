﻿namespace InnopolisIntroToProgAST
{
    public abstract class ArithmeticExpression : Expression
    {
        public abstract long Compute();
    }
}