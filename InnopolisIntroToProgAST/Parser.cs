﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using InnopolisIntroToProgAST.Operators;

namespace InnopolisIntroToProgAST
{
        class Parser
        {
            public string Input { get; private set; }

            public Parser(string s)
            {
                StringBuilder sb = new StringBuilder();
                foreach (char c in s)
                {
                    if (!char.IsWhiteSpace(c))
                    {
                        sb.Append(c);
                    }
                }
                Input = sb.ToString();
            }

            private int iter = -1;


            public Expression Parse()
            {
                Expression answer = ParseAll();
                iter = -1;
                return answer;
            }

            #region parse

            private Expression ParseAll()
            {
                if (!HasNext())
                {
                    return null;
                }
                int balance = 0;
                Stack<Sign> stack = new Stack<Sign>();
                ArrayList lst = new ArrayList();
                while (HasNext() && balance >= 0)
                {
                    char check = Next();
                    if (Next() == '(')
                    {
                        balance++;
                        stack.Push(Bracket.Create(Next().ToString()));
                        MoveNext();
                    }
                    else if (Next() == ')')
                    {
                        balance--;
                        Sign o;
                        while (!((o = stack.Pop()) is Bracket))
                        {
                            lst.Add(o);
                        }
                        MoveNext();
                    }
                    else if (char.IsDigit(Next()))
                    {
                        Integer ex;
                        TryParseInteger(out ex);
                        lst.Add(ex);
                    }
                    else 
                    {
                        Sign code;
                        if (TermOperator.IsSymbol(Next()))
                        {
                            if (!TryParseTermOperator(out code))
                            {
                                throw new Exception("Unknown operator");
                            }
                        }
                        else if (RelationOperator.IsSymbol(Next()))
                        {
                            if (!TryParseRelationOperator(out code))
                            {
                                throw new Exception("Unknown operator");
                            }
                        }
                        else if (LogicalOperator.IsSybmol(Next()))
                        {
                            if (!TryParseLogOperator(out code))
                            {
                                throw new Exception("Unknown Operator");
                            }
                        }
                        else
                        {
                            throw new Exception("Invalid expression");
                        }
                        while (stack.Count != 0 && stack.Peek().Evaluate() >= code.Evaluate())
                        {
                            lst.Add(stack.Pop());
                        }
                        stack.Push(code);
                    }
                }
                while (stack.Count != 0)
                {
                    lst.Add(stack.Pop());
                }
                while (lst.Count > 1)
                {
                    int i = 0;
                    while (lst[i] is Expression)
                    {
                        i++;
                    }
                    Expression left = lst[i - 2] as Expression;
                    Expression right = lst[i - 1] as Expression;
                    Sign op = lst[i] as Sign;
                    lst.RemoveAt(i - 2);
                    lst.RemoveAt(i - 2);
                    lst[i - 2] = Expression.Generate(op, left, right);
                }
                return lst[0] as Expression;
            }
    
            private bool TryParseInteger(out Integer result)
            {
                if (!HasNext())
                {
                    result = null;
                    return false;
                }
                StringBuilder sb = new StringBuilder();
                while (HasNext() && Char.IsDigit(Next()))
                {
                    sb.Append(MoveNext());
                }
                if (sb.Length == 0)
                {
                    result = null;
                    return false;
                }
                long l;
                long.TryParse(sb.ToString(), out l);
                result = new Integer(l);
                return true;
            }

            private bool TryParseLogOperator(out Sign result)
            {
                if (!HasNext())
                {
                    result = null;
                    return false;
                }

                StringBuilder sb = new StringBuilder();
                while (HasNext() && LogicalOperator.IsSybmol(Next()))
                {
                    sb.Append(MoveNext());
                }
                result = LogicalOperator.Create(sb.ToString());
                if (result != null)
                {
                    return true;
                }
                return false;
            }

            private bool TryParseRelationOperator(out Sign result)
            {
                if (!HasNext())
                {
                    result = null;
                    return false;
                }

                StringBuilder sb = new StringBuilder();
                while (HasNext() && RelationOperator.IsSymbol(Next()))
                {
                    sb.Append(MoveNext());
                }
                result = RelationOperator.Create(sb.ToString());
                if (result != null)
                {
                    return true;
                }
                return false;
            }

            private bool TryParseTermOperator(out Sign result)
            {
                if (!HasNext())
                {
                    result = null;
                    return false;
                }
                result = TermOperator.Create(Next().ToString());
                if (result != null)
                {
                    MoveNext();
                    return true;
                }
                return false;
            }


            #endregion

            #region utility


            private char Next()
            {
                if (!HasNext()) throw new IndexOutOfRangeException();
                return Input[iter + 1];
            }

            private bool HasNext()
            {
                if (iter + 1 == Input.Length) return false;
                return true;
            }

            private char MoveNext()
            {
                if (!HasNext()) throw new IndexOutOfRangeException();
                return Input[++iter];
            }
            #endregion
        }
}