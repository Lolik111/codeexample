﻿using System.Xml.Linq;

namespace InnopolisIntroToProgAST
{
    public class Integer : ArithmeticExpression
    {
        public Integer(long value)
        {
            Value = value;
        }

        public Integer(int value)
        {
            this.Value = value;
        }
            
        public long Value { get; private set; }

        public override long Compute()
        {
            return Value;
        }

        public override XElement ToXml()
        {
            XElement node = new XElement("Integer", Value);
            return node;
        }
    }
}