﻿using System;
using System.Reflection.Emit;
using System.Xml.Linq;
using InnopolisIntroToProgAST.Operators;

namespace InnopolisIntroToProgAST
{
    public class Term : ArithmeticExpression
    {
        public Term(TermOperator op, ArithmeticExpression left, ArithmeticExpression right)
        {
            Op = op;
            Left = left;
            Right = right;
        }


        protected Term()
        {
            this.Left = null;
            this.Op = null;
            this.Right = null;
        }

        public TermOperator Op { get; protected set; }

        public ArithmeticExpression Left { get; protected set; }

        public ArithmeticExpression Right { get; protected set; }

        public override long Compute()
        {
            return Op.Apply(Left, Right);
        }

        public override XElement ToXml()
        {
            XElement node = new XElement("Term", new XAttribute("Operator", Op.StringOp));
            node.Add(Left.ToXml());
            node.Add(Right.ToXml());
            return node;
        }
    }
}