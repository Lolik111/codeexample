﻿namespace InnopolisIntroToProgAST.Operators
{
    public class Bracket : Sign
    {
        private Bracket(string str)
        {
            StringOp = str;
        }

        public static Bracket Create(string str)
        {
            switch (str)
            {
                case "(":
                case ")":
                    return new Bracket(str);
                default:
                    return null;
            }
        }

        public override int Evaluate()
        {
            return 0;
        }
    }
}