﻿using System;

namespace InnopolisIntroToProgAST.Operators
{
    public class LogicalOperator : Sign
    {
        private LogicalOperator(string str)
        {
            StringOp = str;
        }

        public static LogicalOperator Create(string str)
        {
            switch (str)
            {
                case "||":
                case "|":
                case "&&":
                case "&":
                    return new LogicalOperator(str);
                default:
                    return null;
            }
        }

        public static bool IsSybmol(char c)
        {
            if (c == '&' || c == '|') return true;
            return false;
        }

        public override int Evaluate()
        {
            switch (StringOp)
            {
                case "||":
                case "|":
                    return 1;
                case "&&":
                case "&":
                    return 2;
                default:
                    return -1;
            }
        }

        public bool Apply(LogicalExpression left, LogicalExpression right)
        {
            switch (StringOp)
            {
                case "|":
                case "||":
                    return left.Compute() | right.Compute();
                case "&":
                case "&&":
                    return left.Compute() && right.Compute();
                default:
                    throw new Exception("Wrong operator");
            }
        }

    }
}