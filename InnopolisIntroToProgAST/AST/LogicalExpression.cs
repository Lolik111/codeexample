﻿namespace InnopolisIntroToProgAST
{
    public abstract class LogicalExpression : Expression
    {
        public abstract bool Compute();
    }
}