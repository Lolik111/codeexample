﻿using System;
using System.Xml.Linq;
using InnopolisIntroToProgAST.Operators;

namespace InnopolisIntroToProgAST
{
    public class Relation : LogicalExpression
    {
        public Relation(RelationOperator op, ArithmeticExpression left, ArithmeticExpression right)
        {
            Op = op;
            Left = left;
            Right = right;
        }

        public RelationOperator Op { get; private set; }

        public ArithmeticExpression Left { get; private set; }

        public ArithmeticExpression Right { get; private set; }


        public override bool Compute()
        {
            return Op.Apply(Left, Right);
        }

        public override XElement ToXml()
        {
            XElement node = new XElement("Relation", new XAttribute("Operator", Op.StringOp));
            node.Add(Left.ToXml());
            node.Add(Right.ToXml());
            return node;
        }
    }
}